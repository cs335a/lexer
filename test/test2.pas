program exRecursion;
var
   num, f: integer;
   // this is a comment
function fact(x: integer): integer; 
begin
   if x=0 then
      fact := 1
   else
      fact := x * fact(x-1); 
end;
begin
   writeln(' Enter a number: ');
   readln(num);
   f := fact(num);
   writeln(' Factorial ', num, ' is: ' , f);
end.
