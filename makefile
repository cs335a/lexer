SHELL := /bin/bash
.PHONY: example assign1 calc
.DEFAULT_GOAL := assign1
example:
	mkdir -p bin
	flex -o src/example1.yy.c src/example1.l
	cc src/example1.yy.c -o bin/example1 -ll
	
assign1:
	mkdir -p bin
	flex -o src/assign1.yy.c src/assign1.l  
	c++ src/assign1.yy.c -o bin/lexer -lfl
	
clean:
	rm -rf bin
	find -name *.out | xargs /bin/rm -f 
cleanall:  clean
	git clean -f -X
#	@echo "-------------------------------"
#	@echo "Are you sure:(y/n)"
#	@read -p "Are you sure:(y/n)" c; \
	ifeq $$c "y"	git clean -f -X; 	endif

calc:
	mkdir -p bin
	flex -o src/calc.yy.c src/calc.l
	bison -d -o src/calc.tab.c src/calc.y
	cc src/calc.yy.c src/calc.tab.c -lfl -o bin/calc
