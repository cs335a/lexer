Letter [a-zA-Z]
DIGIT [0-9]
AN [a-zA-Z0-9_]
space [ \n\t]

%{
#include "stdio.h"
#include "stdlib.h"
#include "iostream"
#include "string"
#define CS_LENGTH_LIMIT 1023
#define CS_MAX_ALLOWED 3
#define N 100000

int ERROR_STATE;
std::string str;
int rewriteString(char *a,int len,int *errorLen){
	char* newString = new char[len];
	char buffer[1000];
	bool isCS = 1;
	char tempInt[CS_MAX_ALLOWED];
	int state = 1,intLen=0,newLen = 0,i = 0;
	for(i=0;i<len;i++){
		if(a[i] != '\'' && !isCS){
			newString[newLen] = a[i];
			newLen++;
		}
		else if(a[i] == '#' && isCS ){
			state = 2;
			if(intLen > 0){
				tempInt[intLen] = '\0';
				newString[newLen] = (char)atoi(tempInt);
				intLen = 0;
				newLen++;
			}
		}
		else if(a[i] == '\''){
			isCS = !isCS;
			if(state == 2){
				tempInt[intLen] = '\0';
				newString[newLen] = (char)atoi(tempInt);
				intLen = 0;
				newLen++;
			}
			state = 1;
			if(a[i-1] == '\'' && !isCS){
				newString[newLen] = '\'';
				newLen++;
			}
		}
		else if(state == 2){
			if(intLen >= CS_MAX_ALLOWED){
				*errorLen = i-CS_MAX_ALLOWED-1;
				sprintf(buffer,"Control String numeral out of bounds at position:%d",*errorLen);
				str.append(buffer,strlen(buffer));
				return CS_LENGTH_LIMIT;
			}
			tempInt[intLen] = a[i];
			intLen++;
		}
	}
	if(state == 2){
		tempInt[intLen] = '\0';
		newString[newLen] = (char)atoi(tempInt);
		intLen = 0;
		newLen++;
	}
	yytext = newString;
	yyleng = newLen;
	yytext[yyleng] = '\0';
//	str.append(yytext,yyleng);
//	str.append(" | ");
	str.append("STRING | ");
	return 0;
}

int modDirective(int len){
	int i;
	for(i= len+2; i< yyleng; i++){
		if(yytext[i]!= ' ' && yytext[i] != '\t' && yytext[i] != '\n')
			break;
	}
	if(yytext[i] == '}'){
		yyleng = 0;
		return 0;
	}
	int tempYT = i;
	for(i = yyleng-2; i>=0; i--){
		if(yytext[i]!=' ' && yytext[i] != '\t' && yytext[i] != '\n')
			break;
	}
	yyleng = i - tempYT + 1;
	yytext = yytext + tempYT;
	//str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
	return 0;
}

%}

%%


"{$"(?i:COPERATORS){space}*[^"}"]*"}"  ECHO; modDirective(10); str.append("'COPERATORS_D' | ");
"{$"(?i:DEFINEC){space}*[^"}"]*"}"  ECHO; modDirective(7); str.append("'DEFINEC_D' | ");
"{$"(?i:DEFINE){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'DEFINE_D' | ");
"{$"(?i:ELSEC){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'ELSEC_D' | ");
"{$"(?i:ELSEIF){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'ELSEIF_D' | ");
"{$"(?i:ELSE){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'ELSE_D' | ");
"{$"(?i:ELIFC){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'ELIFC_D' | ");
"{$"(?i:ENDC){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'ENDC_D' | ");
"{$"(?i:ENDIF){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'ENDIF_D' | ");
"{$"(?i:ERRORC){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'ERRORC_D' | ");
"{$"(?i:ERROR){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'ERROR_D' | ");
"{$"(?i:FATAL){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'FATAL_D' | ");
"{$"(?i:GOTO){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'GOTO_D' | ");
"{$"(?i:H"+"){space}*[^"}"]*"}"  ECHO; modDirective(2); str.append("'HPLUS_D' | ");
"{$"(?i:H"-"){space}*[^"}"]*"}"  ECHO; modDirective(2); str.append("'HMINUS_D' | ");
"{$"(?i:LONGSTRINGS){space}*[^"}"]*"}"  ECHO; modDirective(11); str.append("'LONGSTRINGS_D' | ");
"{$"(?i:IFNDEF){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'IFNDEF_D' | ");
"{$"(?i:IFDEF){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'IFDEF_D' | ");
"{$"(?i:IFOPT){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'IFOPT_D' | ");
"{$"(?i:IFC){space}*[^"}"]*"}"  ECHO; modDirective(3); str.append("'IFC_D' | ");
"{$"(?i:IF){space}*[^"}"]*"}"  ECHO; modDirective(2); str.append("'IF_D' | ");
"{$"(?i:INCLUDEPATH){space}*[^"}"]*"}"  ECHO; modDirective(11); str.append("'INCLUDEPATH_D' | ");
"{$"(?i:INCLUDE){space}*[^"}"]*"}"  ECHO; modDirective(7); str.append("'INCLUDE_D' | ");
"{$"(?i:INFO){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'INFO_D' | ");
"{$"(?i:INLINE){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'INLINE_D' | ");
"{$"(?i:I"+"){space}*[^"}"]*"}"  ECHO; modDirective(2); str.append("'IPLUS_D' | ");
"{$"(?i:I"-"){space}*[^"}"]*"}"  ECHO; modDirective(2); str.append("'IMINUS_D' | ");
"{$"(?i:I){space}*[^"}"]*"}"  ECHO; modDirective(1); str.append("'I_D' | ");
"{$"(?i:LINKLIB){space}*[^"}"]*"}"  ECHO; modDirective(7); str.append("'LINKLIB_D' | ");
"{$"(?i:LINK){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'LINK_D' | ");
"{$"(?i:L){space}*[^"}"]*"}"  ECHO; modDirective(1); str.append("'L_D' | ");
"{$"(?i:MACRO){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'MACRO_D' | ");
"{$"(?i:MESSAGE){space}*[^"}"]*"}"  ECHO; modDirective(7); str.append("'MESSAGE_D' | ");
"{$"(?i:STATIC){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'STATIC_D' | ");
"{$"(?i:STOP){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'STOP_D' | ");
"{$"(?i:UNDEFC){space}*[^"}"]*"}"  ECHO; modDirective(6); str.append("'UNDEFC_D' | ");
"{$"(?i:UNDEF){space}*[^"}"]*"}"  ECHO; modDirective(5); str.append("'UNDEF_D' | ");
"{$"(?i:WARNINGS){space}*[^"}"]*"}"  ECHO; modDirective(8); str.append("'WARNINGS_D' | ");
"{$"(?i:WARNING){space}*[^"}"]*"}"  ECHO; modDirective(7); str.append("'WARNING_D' | ");
"{$"(?i:WARN){space}*[^"}"]*"}"  ECHO; modDirective(4); str.append("'WARN_D' | ");
"{$"(?i:OBJECTPATH){space}*[^"}"]*"}"  ECHO; modDirective(10); str.append("'OBJECTPATH_D' | ");


"{"     ECHO; str.append("'{' | ");
"}"     ECHO; str.append("'}' | ");
"(*"     ECHO; str.append("'(*' | ");
"*)"     ECHO; str.append("'*)' | ");


"//".*     ECHO; str.append("COMMENT | ");

(\'[^'\n]*\'|\#[0-9]+)+  ECHO; rewriteString(yytext,yyleng,&ERROR_STATE);

(?i:end{space}*".") ECHO; str.append("END OF PROGRAM | ");


":=" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"<>" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"<<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
">>" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
">=" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"<=" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"+<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"-<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"+>" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"->" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"*<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"/<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"*>" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"/>" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"**" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"+" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"-" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"*" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"/" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"=" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"<" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
">" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"&" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"|" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"~" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"!" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
":" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
";" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"," 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"(" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
")" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"[" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"]" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"^." 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"^" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
".." 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"." 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 
"@" 	 ECHO; str.append("'"); str.append(yytext,yytext+yyleng); str.append("' | "); 

"$"[0-9a-fA-F]+	ECHO; str.append("HEXADECIMAL | ");
"&"[0-7]+		ECHO; str.append("OCTAL | ");
"%"[01]+ ECHO; str.append("BINARY | "); 
{DIGIT}+"."?{DIGIT}*[eE][+-]?{DIGIT}+ ECHO; str.append("EXPONENTIAL | ");
{DIGIT}+"."{DIGIT}+	     ECHO; str.append("FLOAT | ");
{DIGIT}+"."/[^.]	     ECHO; str.append("FLOAT | ");
{DIGIT}+    ECHO; str.append("INT | ");

(?i:char) 		ECHO; str.append("'CHAR' | ");
(?i:absolute) 	 ECHO; str.append("'ABSOLUTE' | "); 
(?i:and) 	 ECHO; str.append("'AND' | "); 
(?i:array) 	 ECHO; str.append("'ARRAY' | "); 
(?i:asm) 	 ECHO; str.append("'ASM' | "); 
(?i:begin) 	 ECHO; str.append("'BEGIN' | "); 
(?i:case) 	 ECHO; str.append("'CASE' | "); 
(?i:const) 	 ECHO; str.append("'CONST' | "); 
(?i:constructor) 	 ECHO; str.append("'CONSTRUCTOR' | "); 
(?i:destructor) 	 ECHO; str.append("'DESTRUCTOR' | "); 
(?i:div) 	 ECHO; str.append("'DIV' | "); 
(?i:do) 	 ECHO; str.append("'DO' | "); 
(?i:downto) 	 ECHO; str.append("'DOWNTO' | "); 
(?i:else) 	 ECHO; str.append("'ELSE' | "); 
(?i:end) 	 ECHO; str.append("'END' | "); 
(?i:file) 	 ECHO; str.append("'FILE' | "); 
(?i:for) 	 ECHO; str.append("'FOR' | "); 
(?i:function) 	 ECHO; str.append("'FUNCTION' | "); 
(?i:goto) 	 ECHO; str.append("'GOTO' | "); 
(?i:if) 	 ECHO; str.append("'IF' | "); 
(?i:implementation) 	 ECHO; str.append("'IMPLEMENTATION' | "); 
(?i:in) 	 ECHO; str.append("'IN' | "); 
(?i:inherited) 	 ECHO; str.append("'INHERITED' | "); 
(?i:inline) 	 ECHO; str.append("'INLINE' | "); 
(?i:interface) 	 ECHO; str.append("'INTERFACE' | "); 
(?i:label) 	 ECHO; str.append("'LABEL' | "); 
(?i:mod) 	 ECHO; str.append("'MOD' | "); 
(?i:nil) 	 ECHO; str.append("'NIL' | "); 
(?i:not) 	 ECHO; str.append("'NOT' | "); 
(?i:object) 	 ECHO; str.append("'OBJECT' | "); 
(?i:of) 	 ECHO; str.append("'OF' | "); 
(?i:operator) 	 ECHO; str.append("'OPERATOR' | "); 
(?i:or) 	 ECHO; str.append("'OR' | "); 
(?i:procedure) 	 ECHO; str.append("'PROCEDURE' | "); 
(?i:program) 	 ECHO; str.append("'PROGRAM' | "); 
(?i:record) 	 ECHO; str.append("'RECORD' | "); 
(?i:reintroduce) 	 ECHO; str.append("'REINTRODUCE' | "); 
(?i:repeat) 	 ECHO; str.append("'REPEAT' | "); 
(?i:self) 	 ECHO; str.append("'SELF' | "); 
(?i:set) 	 ECHO; str.append("'SET' | "); 
(?i:shl) 	 ECHO; str.append("'SHL' | "); 
(?i:shr) 	 ECHO; str.append("'SHR' | "); 
(?i:string) 	 ECHO; str.append("'STRING' | "); 
(?i:then) 	 ECHO; str.append("'THEN' | "); 
(?i:to) 	 ECHO; str.append("'TO' | "); 
(?i:type) 	 ECHO; str.append("'TYPE' | "); 
(?i:unit) 	 ECHO; str.append("'UNIT' | "); 
(?i:until) 	 ECHO; str.append("'UNTIL' | "); 
(?i:uses) 	 ECHO; str.append("'USES' | "); 
(?i:var) 	 ECHO; str.append("'VAR' | "); 
(?i:while) 	 ECHO; str.append("'WHILE' | "); 
(?i:with) 	 ECHO; str.append("'WITH' | "); 
(?i:xor) 	 ECHO; str.append("'XOR' | "); 
(?i:dispose) 	 ECHO; str.append("'DISPOSE' | "); 
(?i:false) 	 ECHO; str.append("'FALSE' | "); 
(?i:true) 	 ECHO; str.append("'TRUE' | "); 
(?i:exit) 	 ECHO; str.append("'EXIT' | "); 
(?i:new) 	 ECHO; str.append("'NEW' | "); 
(?i:as) 	 ECHO; str.append("'AS' | "); 
(?i:class) 	 ECHO; str.append("'CLASS' | "); 
(?i:dispinterface) 	 ECHO; str.append("'DISPINTERFACE' | "); 
(?i:except) 	 ECHO; str.append("'EXCEPT' | "); 
(?i:exports) 	 ECHO; str.append("'EXPORTS' | "); 
(?i:finalization) 	 ECHO; str.append("'FINALIZATION' | "); 
(?i:finally) 	 ECHO; str.append("'FINALLY' | "); 
(?i:initialization) 	 ECHO; str.append("'INTIALIZATION' | "); 
(?i:is) 	 ECHO; str.append("'IS' | "); 
(?i:library) 	 ECHO; str.append("'LIBRARY' | "); 
(?i:on) 	 ECHO; str.append("'ON' | "); 
(?i:out) 	 ECHO; str.append("'OUT' | "); 
(?i:bitpacked) 	 ECHO; str.append("'BITPACKED' | "); 
(?i:properly) 	 ECHO; str.append("'PROPERLY' | "); 
(?i:raise) 	 ECHO; str.append("'RAISE' | "); 
(?i:resourcestring) 	 ECHO; str.append("'RESOURCESTRING' | "); 
(?i:threadvar) 	 ECHO; str.append("'THREADVAR' | "); 
(?i:try) 	 ECHO; str.append("'TRY' | "); 
(?i:abstract) 	 ECHO; str.append("'ABSTRACT' | "); 
(?i:alias) 	 ECHO; str.append("'ALIAS' | "); 
(?i:assembler) 	 ECHO; str.append("'ASSEMBLER' | "); 
(?i:packed) 	 ECHO; str.append("'PACKED' | "); 
(?i:break) 	 ECHO; str.append("'BREAK' | "); 
(?i:cdecl) 	 ECHO; str.append("'CDECL' | "); 
(?i:continue) 	 ECHO; str.append("'CONTINUE' | "); 
(?i:cppdecl) 	 ECHO; str.append("'CPPDECL' | "); 
(?i:cvar) 	 ECHO; str.append("'CVAR' | "); 
(?i:default) 	 ECHO; str.append("'DEFAULT' | "); 
(?i:deprecated) 	 ECHO; str.append("'DEPRECATED' | "); 
(?i:dynamic) 	 ECHO; str.append("'DYNAMIC' | "); 
(?i:enumerator) 	 ECHO; str.append("'ENUMERATOR' | "); 
(?i:experimental) 	 ECHO; str.append("'EXPERIMENTAL' | "); 
(?i:export) 	 ECHO; str.append("'EXPORT' | "); 
(?i:external) 	 ECHO; str.append("'EXTERNAL' | "); 
(?i:far) 	 ECHO; str.append("'FAR' | "); 
(?i:far16) 	 ECHO; str.append("'FAR16' | "); 
(?i:forward) 	 ECHO; str.append("'FORWARD' | "); 
(?i:generic) 	 ECHO; str.append("'GENERIC' | "); 
(?i:helper) 	 ECHO; str.append("'HELPER' | "); 
(?i:implements) 	 ECHO; str.append("'IMPLEMENTS' | "); 
(?i:index) 	 ECHO; str.append("'INDEX' | "); 
(?i:interrupt) 	 ECHO; str.append("'INTERRUPT' | "); 
(?i:iochecks) 	 ECHO; str.append("'IOCHECKS' | "); 
(?i:local) 	 ECHO; str.append("'LOCAL' | "); 
(?i:message) 	 ECHO; str.append("'MESSAGE' | "); 
(?i:name) 	 ECHO; str.append("'NAME' | "); 
(?i:near) 	 ECHO; str.append("'NEAR' | "); 
(?i:nodefault) 	 ECHO; str.append("'NODEFAULT' | "); 
(?i:noreturn) 	 ECHO; str.append("'NORETURN' | "); 
(?i:nostackframe) 	 ECHO; str.append("'NOSTACKFRAME' | "); 
(?i:oldfpccall) 	 ECHO; str.append("'OLDFPCCALL' | "); 
(?i:otherwise) 	 ECHO; str.append("'OTHERWISE' | "); 
(?i:overload) 	 ECHO; str.append("'OVERLOAD' | "); 
(?i:override) 	 ECHO; str.append("'OVERRIDE' | "); 
(?i:pascal) 	 ECHO; str.append("'PASCAL' | "); 
(?i:platform) 	 ECHO; str.append("'PLATFORM' | "); 
(?i:private) 	 ECHO; str.append("'PRIVATE' | "); 
(?i:protected) 	 ECHO; str.append("'PROTECTED' | "); 
(?i:public) 	 ECHO; str.append("'PUBLIC' | "); 
(?i:published) 	 ECHO; str.append("'PUBLISHED' | "); 
(?i:read) 	 ECHO; str.append("'READ' | "); 
(?i:register) 	 ECHO; str.append("'REGISTER' | "); 
(?i:result) 	 ECHO; str.append("'RESULT' | "); 
(?i:safecall) 	 ECHO; str.append("'SAFECALL' | "); 
(?i:saveregisters) 	 ECHO; str.append("'SAVEREGISTERS' | "); 
(?i:softfloat) 	 ECHO; str.append("'SOFTFLOAT' | "); 
(?i:specialize) 	 ECHO; str.append("'SPECIALIZE' | "); 
(?i:static) 	 ECHO; str.append("'STATIC' | "); 
(?i:stdcall) 	 ECHO; str.append("'STDCALL' | "); 
(?i:stored) 	 ECHO; str.append("'STORED' | "); 
(?i:strict) 	 ECHO; str.append("'STRICT' | "); 
(?i:unalligned) 	 ECHO; str.append("'UNALLIGNED' | "); 
(?i:unimplemented) 	 ECHO; str.append("'UNIMPLEMENTED' | "); 
(?i:varargs) 	 ECHO; str.append("'VARARGS' | "); 
(?i:virtual) 	 ECHO; str.append("'VIRTUAL' | "); 
(?i:write) 	 ECHO; str.append("'WRITE' | "); 
(?i:integer) 	 ECHO; str.append("'INTEGER' | "); 
(?i:cardinal) 	 ECHO; str.append("'CARDINAL' | "); 
(?i:shortint) 	 ECHO; str.append("'SHORTINT' | "); 
(?i:smallint) 	 ECHO; str.append("'SMALLINT' | "); 
(?i:longint) 	 ECHO; str.append("'LONGINT' | "); 
(?i:int64) 	 ECHO; str.append("'INT64' | "); 
(?i:byte) 	 ECHO; str.append("'BYTE' | "); 
(?i:longword) 	 ECHO; str.append("'LONGWORD' | "); 
(?i:word) 	 ECHO; str.append("'WORD' | "); 
(?i:extended) 	 ECHO; str.append("'EXTENDED' | "); 



"&"?[a-zA-Z_]{AN}*     ECHO; str.append("IDENTIFIER | ");

\n  { 			
				if(str.length() > 2){
					str.erase(str.end()-2,str.end());
		    		std::cout << "\t\t{ " << str << "}";
		    	}
		    	str.clear();
		    	ECHO;
	}    	

<<EOF>> { 
			if(str.length() > 2){
				str.erase(str.end()-2,str.end());
		    	std::cout << "\t\t{ " << str << "}";
		    }
		    return 0;
		}

[ \t]    ECHO;

["&""$""%"]?{DIGIT}+[a-zA-Z_]{AN}*	ECHO; str.append("BAD_IDENTIFIER | ");

.             ECHO; str.append("ALIEN:\""); str.append(yytext,yytext+yyleng); str.append("\" | ");
%%


int main(int argv, char *argc[]){
	yyin = fopen(argc[1],"r");
	yylex();
	fclose(yyin);
	return 0;
}